package com.anubytes.account.repository;

import com.anubytes.account.entity.db.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends BaseRepository<Account, Long>{
    List<Account> findByCustomerId(Long customerId);
}
