package com.anubytes.account.repository;

import com.anubytes.account.entity.db.Customer;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends BaseRepository<Customer, Long>{

    Optional<Customer> findByMobileNumber(String mobileNumber);
}
