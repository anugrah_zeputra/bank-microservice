package com.anubytes.account.controller;

import com.anubytes.account.dto.CustomerDto;
import com.anubytes.account.dto.response.AccountResponse;
import com.anubytes.account.service.app.IAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path ="/api/v1", produces = {MediaType.APPLICATION_JSON_VALUE})
public class AccountController {

    private final IAccountService accountService;

    @PostMapping("/create")
    public ResponseEntity<?> createAccount(@RequestBody CustomerDto request) {
        accountService.createCustomer(request);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(AccountResponse.builder()
                        .statusMsg("Success - Account Created Successfully")
                        .statusCode("201")
                        .statusException("")
                        .build()
                );
    }

    @GetMapping("/get")
    public ResponseEntity<?> getAccountByMobileNumber(@RequestParam String mobileNumber) {
        return ResponseEntity
                .status(HttpStatus.FOUND)
                .body(accountService.getAccountDetails(mobileNumber));
    }
}
