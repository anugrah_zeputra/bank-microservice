package com.anubytes.account.dto.mapper;

import com.anubytes.account.dto.AccountDto;
import com.anubytes.account.entity.db.Account;

public class AccountMapper {

    public static Account mapToAccount(AccountDto request, Account account){
        account.setCustomerId(request.getCustomerId());
        account.setAccountNumber(request.getAccountNumber());
        account.setAccountType(request.getAccountType());
        account.setBranchAddress(request.getBranchAddress());
        return account;
    }

    public static AccountDto maptToAccountRequest(Account account, AccountDto request){
        request.setCustomerId(account.getCustomerId());
        request.setAccountNumber(account.getAccountNumber());
        request.setAccountType(account.getAccountType());
        request.setBranchAddress(account.getBranchAddress());
        return request;
    }

}
