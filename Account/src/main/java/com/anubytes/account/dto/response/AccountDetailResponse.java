package com.anubytes.account.dto.response;

import com.anubytes.account.dto.AccountDto;
import com.anubytes.account.dto.CustomerDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data @AllArgsConstructor @Builder
public class AccountDetailResponse {

    private String statusCode;
    private String statusMsg;
    private String statusException;
    private CustomerDto customer;
    private List<AccountDto> accounts;
}
