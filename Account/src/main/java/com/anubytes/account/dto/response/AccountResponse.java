package com.anubytes.account.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data @AllArgsConstructor @Builder
public class AccountResponse {
    private String statusCode;
    private String statusMsg;
    private String statusException;
}
