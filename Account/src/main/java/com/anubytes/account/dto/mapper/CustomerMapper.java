package com.anubytes.account.dto.mapper;

import com.anubytes.account.dto.CustomerDto;
import com.anubytes.account.entity.db.Customer;

public class CustomerMapper {

    public static Customer mapToCustomer(CustomerDto request, Customer customer){
        customer.setCustomerId(request.getCustomerId());
        customer.setName(request.getName());
        customer.setEmail(request.getEmail());
        customer.setMobileNumber(request.getMobileNumber());
        return customer;
    }

    public static CustomerDto mapToCustomerDto(Customer customer, CustomerDto request){
        request.setCustomerId(customer.getCustomerId());
        request.setName(customer.getName());
        request.setEmail(customer.getEmail());
        request.setMobileNumber(customer.getMobileNumber());
        return request;
    }
}
