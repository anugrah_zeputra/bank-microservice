package com.anubytes.account.dto;

import lombok.*;

@Data
public class AccountDto {
    private Long customerId;
    private Long accountNumber;
    private String accountType;
    private String branchAddress;
}
