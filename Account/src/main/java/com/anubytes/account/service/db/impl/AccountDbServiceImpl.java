package com.anubytes.account.service.db.impl;

import com.anubytes.account.entity.db.Account;
import com.anubytes.account.repository.AccountRepository;
import com.anubytes.account.service.db.IAccountDbService;
import com.anubytes.account.utils.Helper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class AccountDbServiceImpl implements IAccountDbService {

    private final AccountRepository accountRepository;

    /**
     * @param account - Account
     * @return Account
     */
    @Override
    public Account saveOrUpdate(Account account) {
        account.setCreatedBy(Helper.changeObjectIfNull(account.getCreatedBy(), "Anonymous"));
        account.setCreatedAt(LocalDateTime.now());
        account.setModifiedBy(Helper.changeObjectIfNull(account.getModifiedBy(), "Anonymous"));
        account.setModifiedAt(LocalDateTime.now());
        return accountRepository.save(account);
    }

    /**
     * @return List Account
     */
    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    /**
     * @param identity - Long
     * @return Account
     */
    @Override
    public Account getById(Long identity) {
        return accountRepository.findById(identity).orElse(null);
    }

    /**
     * @param account - Account
     * @return Boolean
     */
    @Override
    public Boolean delete(Account account) {
        try {
            accountRepository.delete(account);
            return true;
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    /**
     * @param customerId - Long
     * @return Account
     */
    @Override
    public List<Account> findByCustomerId(Long customerId) {
        return accountRepository.findByCustomerId(customerId);
    }
}
