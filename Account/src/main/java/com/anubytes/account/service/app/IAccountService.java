package com.anubytes.account.service.app;

import com.anubytes.account.dto.CustomerDto;
import com.anubytes.account.dto.response.AccountDetailResponse;

public interface IAccountService {

    /**
     * @param mobileNumber - String
     * @return AccountDetailResponse
     */
    AccountDetailResponse getAccountDetails(String mobileNumber);

    /**
     * @param request - CustomerRequest
     */
    void createCustomer(CustomerDto request);
}
