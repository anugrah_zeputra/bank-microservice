package com.anubytes.account.service.app.impl;

import com.anubytes.account.dto.AccountDto;
import com.anubytes.account.dto.mapper.AccountMapper;
import com.anubytes.account.dto.mapper.CustomerMapper;
import com.anubytes.account.dto.CustomerDto;
import com.anubytes.account.dto.response.AccountDetailResponse;
import com.anubytes.account.entity.db.Account;
import com.anubytes.account.entity.db.Customer;
import com.anubytes.account.exception.ValidationException;
import com.anubytes.account.service.app.IAccountService;
import com.anubytes.account.service.db.IAccountDbService;
import com.anubytes.account.service.db.ICustomerDbService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements IAccountService {

    private final IAccountDbService accountDbService;
    private final ICustomerDbService customerDbService;

    /**
     * @param mobileNumber - String
     * @return AccountDetailResponse
     */
    @Override
    public AccountDetailResponse getAccountDetails(String mobileNumber) {
        Customer customer = customerDbService.findByMobileNumber(mobileNumber);
        if(customer == null) {
            throw new ValidationException("Failed - Customer not found with mobile number: " + mobileNumber);
        }
        List<Account> account = accountDbService.findByCustomerId(customer.getCustomerId());
        return AccountDetailResponse.builder()
                .statusCode("302")
                .statusMsg("Success - Fetch Data By Mobile Number: " + mobileNumber)
                .statusException("")
                .customer(CustomerMapper.mapToCustomerDto(customer, new CustomerDto()))
                .accounts(account.stream().map((v) -> AccountMapper.maptToAccountRequest(v, new AccountDto())).toList())
                .build();
    }

    /**
     * @param request - CustomerRequest
     */
    @Override
    public void createCustomer(CustomerDto request) {
        Customer customer = CustomerMapper.mapToCustomer(request, new Customer());
        Customer customerByMobileNumber = customerDbService.findByMobileNumber(customer.getMobileNumber());
        if(customerByMobileNumber != null){
            throw new ValidationException("Failed - Customer already exists with mobile number: " + customer.getMobileNumber());
        }
        Customer newSavedCustomer = customerDbService.saveOrUpdate(customer);
        Account account = createNewAccount(newSavedCustomer);
        accountDbService.saveOrUpdate(account);
    }

    private Account createNewAccount(Customer customer) {
        return Account.builder()
                .accountNumber(1_00_000_000L + new Random().nextInt(9_00_000_000))
                .accountType("Savings")
                .branchAddress("Jakarta")
                .customerId(customer.getCustomerId())
                .build();
    }
}
