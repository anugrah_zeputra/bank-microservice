package com.anubytes.account.service.db.impl;

import com.anubytes.account.entity.db.Customer;
import com.anubytes.account.repository.CustomerRepository;
import com.anubytes.account.service.db.ICustomerDbService;
import com.anubytes.account.utils.Helper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class CustomerDbServiceImpl implements ICustomerDbService {

    private final CustomerRepository customerRepository;

    /**
     * @param customer - Customer
     * @return customer
     */
    @Override
    public Customer saveOrUpdate(Customer customer) {
        customer.setCreatedBy(Helper.changeObjectIfNull(customer.getCreatedBy(), "Anonymous"));
        customer.setCreatedAt(LocalDateTime.now());
        customer.setModifiedBy(Helper.changeObjectIfNull(customer.getModifiedBy(), "Anonymous"));
        customer.setModifiedAt(LocalDateTime.now());
        return customerRepository.save(customer);
    }

    /**
     * @return List Customer
     */
    @Override
    public List<Customer> getAll() {
        return customerRepository.findAll();
    }

    /**
     * @param identity - Long
     * @return Customer
     */
    @Override
    public Customer getById(Long identity) {
        return customerRepository.findById(identity).orElse(null);
    }

    /**
     * @param customer - Customer
     * @return Boolean
     */
    @Override
    public Boolean delete(Customer customer) {
        try {
            customerRepository.delete(customer);
            return true;
        } catch (Exception e) {
            e.fillInStackTrace();
            return false;
        }
    }

    /**
     * @param mobileNumber - String
     * @return Customer
     */
    @Override
    public Customer findByMobileNumber(String mobileNumber) {
        return customerRepository.findByMobileNumber(mobileNumber).orElse(null);
    }
}
