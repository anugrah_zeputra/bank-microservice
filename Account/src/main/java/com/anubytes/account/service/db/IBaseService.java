package com.anubytes.account.service.db;

import java.util.List;

public interface IBaseService<T, ID> {
    T saveOrUpdate(T t);
    List<T> getAll();
    T getById(ID identity);
    Boolean delete(T t);

}
