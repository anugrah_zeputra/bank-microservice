package com.anubytes.account.service.db;

import com.anubytes.account.entity.db.Customer;

public interface ICustomerDbService extends IBaseService<Customer, Long> {
    Customer findByMobileNumber(String mobileNumber);
}
