package com.anubytes.account.service.db;

import com.anubytes.account.entity.db.Account;

import java.util.List;

public interface IAccountDbService extends IBaseService<Account, Long>{
    /**
     * @param customerId - Long
     * @return Account
     */
    List<Account> findByCustomerId(Long customerId);
}
