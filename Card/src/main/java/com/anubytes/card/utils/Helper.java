package com.anubytes.card.utils;

import java.util.Objects;

public class Helper {
    private Helper() {

    }
    public static <T> T changeObjectIfNull(T object, T newObject){
        return Objects.nonNull(object) ? object : newObject;
    }
}
